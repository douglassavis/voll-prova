<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TemplateResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'template_id' => $this->template_id,
            'name' => $this->name,
            'subject' => $this->subject,
            'body' => $this->body,
            'created_user_id' => $this->created_user_id,
            'updated_user_id' => $this->updated_user_id,
            'deleted_user_id' => $this->deleted_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
