<?php

namespace App\Http\Controllers\Api;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Template;
use App\Http\Controllers\Controller;
use App\Http\Resources\TemplateResource;

class TemplateController extends Controller
{
	public function index()
	{
		$templates = Template::all();
		return TemplateResource::collection($templates);
	}

	public function show($id)
	{
		$template = Template::findOrFail($id);
		return new TemplateResource($template);
	}
 
    public function store(Request $request)
    {
        $template = $request->isMethod('put') ? Template::findOrFail($request->user_id) : new Task;
            
        $template->user_id = $request->input('user_id');
        $template->name = $request->input('name');
        $template->email = $request->input('email');
        $template->is_active = $request->input('is_active');
 
        if($template->save()) {
            return new TemplateResource($template);
        }
    }

    public function destroy($id)
    {
        $template = Template::findOrfail($id); 
        if($template->delete()) {
            return new TemplateResource($template);
        } 
 
    }
}
