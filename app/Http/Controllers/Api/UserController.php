<?php

namespace App\Http\Controllers\Api;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
	public function index()
	{
		$users = User::all();
		return UserResource::collection($users);
	}

	public function show($id)
	{
		$user = User::findOrFail($id);
		return new UserResource($user);
	}
 
    public function store(Request $request)
    {
        $user = $request->isMethod('put') ? User::findOrFail($request->user_id) : new Task;
            
        $user->user_id = $request->input('user_id');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->is_active = $request->input('is_active');
 
        if($user->save()) {
            return new UserResource($user);
        }
    }

    public function destroy($id)
    {
        $user = User::findOrfail($id); 
        if($user->delete()) {
            return new UserResource($user);
        } 
 
    }
}
