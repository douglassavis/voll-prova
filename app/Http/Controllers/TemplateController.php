<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;
use App\Template;
use App\Queue;

class TemplateController extends Controller
{
	public function index()
	{
		$result = Template::with(['create_user', 'update_user', 'delete_user'])->latest()->paginate();

		return view('template.index', compact('result'));
	}

	public function create()
	{
		return view('template.new');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'bail|required|min:2',
			'subject' => 'required|string',
			'body' => 'required|string',
		]);

		// Add fields
		$request->request->add([ 'created_user_id' => Auth::user()->user_id ]);

		// Create the template
		if ( $template = Template::create($request->only('name', 'subject', 'body', 'created_user_id')) ) {
			flash('Template has been created.');
		} else {
			flash()->error('Unable to create template.');
		}

		return redirect()->route('templates.index');
	}

	public function edit($id)
	{
		$template = Template::find($id);

		return view('template.edit', compact('template'));
	}

	public function update(Request $request, $id)
	{

		$this->validate($request, [
			'name' => 'bail|required|min:2',
			'subject' => 'required|string',
			'body' => 'required|string',
		]);

		// Get the template
		$template = Template::findOrFail($id);

		// Add fields
		$request->request->add([ 'updated_user_id' => Auth::user()->user_id ]);

		// Update template
		$template->fill($request->only('name', 'subject', 'body', 'updated_user_id'));

		$template->save();

		flash()->success('Template has been updated.');
		return redirect()->route('templates.index');
	}

	public function destroy($id)
	{
		try {

			$template = Template::findOrFail($id);

			if($template->delete()){
				DB::table('templates')->where('template_id', $id)->update([ 'deleted_user_id' => Auth::user()->user_id ]);

				flash()->success('Template has been deleted');
			}

			return redirect()->back();

		} catch (Exception $e) {

			flash()->success('Template not deleted');
			return redirect()->back();

		}

	}

	public function sendEmail($id)
	{
		$template = Template::find($id);

		return view('template.send_email', compact('template'));
	}

	public function postSendEmail(Request $request)
	{
		$this->validate($request, [
			'template_id' => 'required|integer',
			'email' => 'required|email',
		]);

		// Add fields
		$request->request->add([ 'created_user_id' => Auth::user()->user_id, 'status' => 0 ]);

		// Insert email queue
		if ( $template = Queue::create($request->only('template_id', 'email', 'body', 'created_user_id', 'status')) ) {
			flash('Email added to the queue.');
		} else {
			flash()->error('Unable to add the email to the queue.');
		}

		return redirect()->route('templates.index');
	}
}
