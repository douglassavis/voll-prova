<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;
use App\Queue;

class QueueController extends Controller
{
	public function index()
	{
		$result = Queue::with(['template'])->latest()->paginate();

		return view('queue.index', compact('result'));
	}

	public function destroy($id)
	{
		try {

			$queue = Queue::findOrFail($id);

			if($queue->delete()){
				DB::table('queues')->where('queue_id', $id)->update([ 'deleted_user_id' => Auth::user()->user_id ]);

				flash()->success('Email removed from queue.');
			}

			return redirect()->back();

		} catch (Exception $e) {

			flash()->success('Unable to remove email from queue.');
			return redirect()->back();

		}

	}

	public function attempt($id = '')
	{
		try {

			if(isset($id) && $id != '')
				$queues = Queue::with(['template'])->where([ 'queue_id' => $id, 'status' => 0 ])->get();
			else
				$queues = Queue::with(['template'])->where([ 'status' => 0 ])->get();


			foreach ($queues as $queue) {

				$queue_id = $queue->queue_id;
		        $title = $queue->template->subject;
		        $content = $queue->template->body;

		        $recipient = $queue->email;

		        Mail::send('emails.send', ['content' => $content], function ($message) use ($title, $recipient)
		        {

		            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));

		            $message->to($recipient);

		            $message->subject($title);

		        });

		        Queue::find($queue_id)->update([ 'last_attempt' => Carbon::now(), 'status' => 1, 'attempt_user_id' => Auth::user()->user_id ]);

		    }

			flash('Processed queue.');

			return redirect()->route('queue.index');

		} catch (Exception $e) {

			flash()->success('Unable to process the queue.');
			return redirect()->back();

		}
	}
}
