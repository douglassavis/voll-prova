<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Queue extends Model
{
	use HasRoles;
    use SoftDeletes;

	protected $guard_name = 'web';

    // Do not set updated_at timestamp.
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_id', 'email', 'status', 'last_attempt', 'created_user_id', 'attempt_user_id', 'deleted_user_id', 'created_at'
    ];


    /**
     * The primary key from table.
     *
     * @var array
     */
    protected $primaryKey = 'queue_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];


	public function create_user()
	{
		return $this->belongsTo(User::class, 'created_user_id');
	}

    public function attempt_user()
    {
        return $this->belongsTo(User::class, 'attempt_user_id');
    }

	public function delete_user()
	{
		return $this->belongsTo(User::class, 'deleted_user_id');
	}


    public function template()
    {
        return $this->belongsTo(Template::class, 'template_id');
    }
}
