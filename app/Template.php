<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Template extends Model
{
	use HasRoles;
    use SoftDeletes;

	protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'subject', 'body', 'created_user_id', 'updated_user_id', 'deleted_user_id'
    ];


    /**
     * The primary key from table.
     *
     * @var array
     */
    protected $primaryKey = 'template_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


	public function create_user()
	{
		return $this->belongsTo(User::class, 'created_user_id');
	}

	public function update_user()
	{
		return $this->belongsTo(User::class, 'updated_user_id');
	}

	public function delete_user()
	{
		return $this->belongsTo(User::class, 'deleted_user_id');
	}


    public function queues()
    {
      return $this->hasMany(Queue::class, 'template_id');
    }
}
