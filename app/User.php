<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Template;

class User extends Authenticatable
{
    use Notifiable;
    use Authorizable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * The primary key from table.
     *
     * @var array
     */
    protected $primaryKey = 'user_id';


    /**
     * The attributes with default value.
     *
     * @var array
     */
    protected $attributes = [
        'is_active' => 1,
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function create_templates()
    {
      return $this->hasMany(Template::class, 'created_user_id');
    }

    public function update_templates()
    {
      return $this->hasMany(Template::class, 'updated_user_id');
    }

    public function delete_templates()
    {
      return $this->hasMany(Template::class, 'deleted_user_id');
    }


    public function create_queues()
    {
      return $this->hasMany(Queue::class, 'created_user_id');
    }

    public function attempt_queues()
    {
      return $this->hasMany(Queue::class, 'attempt_user_id');
    }

    public function delete_queues()
    {
      return $this->hasMany(Queue::class, 'deleted_user_id');
    }
}
