<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (Auth::check())
    	return Redirect::to('/home');
    else
    	return Redirect::to('/login');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group( ['middleware' => ['auth']], function() {

	Route::resource('users', 'UserController');

	Route::get('/templates/{id}/send-email', 'TemplateController@sendEmail')->name('templates.send_email');
	Route::post('/templates/send-email', 'TemplateController@postSendEmail')->name('templates.send_email.post');
	Route::resource('templates', 'TemplateController');

	Route::post('/queue/attempt/{id?}', 'QueueController@attempt')->name('queue.attempt');
	Route::resource('queue', 'QueueController', ['only' => [ 'index', 'destroy' ]]);

});
