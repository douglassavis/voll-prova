<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Controllers Within The "App\Http\Controllers\Api" Namespace
Route::group(['namespace' => 'Api'], function (){

	//Route::group(['middleware' => ['auth:api']], function() {

		// Users
		// *****

		// get list
		Route::get('users','UserController@index');
		// get specific
		Route::get('user/{id}','UserController@show');
		// create new
		Route::post('user','UserController@store');
		// update existing
		Route::put('user','UserController@store');
		// delete specific
		Route::delete('user/{id}','UserController@destroy');


		// Templates
		// *****

		// get list
		Route::get('templates','TemplateController@index');
		// get specific
		Route::get('template/{id}','TemplateController@show');
		// create new
		Route::post('template','TemplateController@store');
		// update existing
		Route::put('template','TemplateController@store');
		// delete specific
		Route::delete('template/{id}','TemplateController@destroy');

	//});

});

