<?php

return [

    /*
     *
     *  Código de erro:
     *    Primeira letra = Nome do Portal
     *       M - Monitoramento
     *       C - CCO
     *       U - Usuário
     *       P - PDV
     *       A - Administração
     *       F - Multa  *diferente
     *       N - Notificação
     *       S - Site
     *       W - Webapp
     *    Seguido de "001-"
     *    Seguido da letra da Controller, ex:
     *       D - Dashboard
     *       G - Gerarotativo
     *     Seguido de um número sequencial
     *
     *  Nome da variável:
     *    Informar o nome da stored procedure seguido de "_amigavel" e "_interno"
     *
     */

    'retorno_usuario_amigavel'            =>     'Erro ao efetuar solicitação!',
    'retorno_usuario_interno'             =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P001.',
    'lista_estados_amigavel'              =>     'Erro ao buscar os dados.',
    'lista_estados_interno'               =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P002.',
    'lista_municipios_amigavel'           =>     'Erro ao buscar os dados.',
    'lista_municipios_interno'            =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P003.',
    'campos_vazios_amigavel'              =>     'Campos nome ou mensagem sem preenchimento.',
    'campos_vazios_interno'               =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P004.',
    'envia_email_amigavel'                =>     'Erro ao efetuar solicitação!',
    'envia_email_interno'                 =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P005.',
    'anexe_curriculo_amigavel'            =>     'Por favor: Anexe um curriculo',
    'anexe_curriculo_interno'             =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P006.',
    'nome_invalido_amigavel'              =>     'Por favor: Insira um nome valido. O nome deve conter no mínino 4 caracteres',
    'nome_invalido_interno'               =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P007.',
    'telefone_invalido_amigavel'          =>     'Por favor: Insira um telefone válido',
    'telefone_invalido_interno'           =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P008.',
    'arquivo_invalido_amigavel'           =>     'Tamanho do arquivo invalido seu arquivo e superior a 1MB',
    'arquivo_invalido_interno'            =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P009.',
    'formato_invalido_amigavel'           =>     'Formato do arquivo invalido! Os formatos permitidos são .doc .docx .pdf',
    'formato_invalido_interno'            =>     'Em caso de dúvidas entre em contato, pelo fale conosco, informando o código de erro S001-P009.',
    

];
