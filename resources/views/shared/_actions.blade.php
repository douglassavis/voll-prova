{{--
@can('preview_'.$entity)
    <a href="{{ route($entity.'.edit', [str_singular($entity) => $id])  }}" class="btn btn-sm btn-info">
        <i class="fas fa-search"></i></i> Visualizar
    </a>
@endcan
--}}

@if($entity=='templates')
    @can('edit_'.$entity)
        <a href="{{ route($entity.'.edit', [str_singular($entity) => $id])  }}" class="btn btn-sm btn-primary">
            <i class="fas fa-pen"></i></i> Editar
        </a>
    @endcan

    @can('delete_'.$entity)
        {!! Form::open( ['method' => 'delete', 'url' => route($entity.'.destroy', [str_singular($entity) => $id]), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Confirma a exclusão do registro?")']) !!}
            <button type="submit" class="btn-delete btn btn-sm btn-danger">
                <i class="fas fa-trash"></i> Excluir
            </button>
        {!! Form::close() !!}
    @endcan


    @can('send_email'.$entity)
        <a href="{{ route($entity.'.send_email', [str_singular($entity) => $id])  }}" class="btn btn-sm btn-warning">
            <i class="fas fa-paper-plane"></i></i> Enviar
        </a>
    @endcan
@endif


@if($entity=='queue')
    @can('start_queues'.$entity)
        <a class="btn btn-sm btn-warning" href="{{ route('queue.attempt', ['id' => $id])  }}" onclick="event.preventDefault(); document.getElementById('attempt-{{ $id }}').submit();">
            <i class="fas fa-paper-plane"></i></i> Enviar
        </a>
        <form id="attempt-{{ $id }}" action="{{ route('queue.attempt', ['id' => $id])  }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    @endcan
@endif