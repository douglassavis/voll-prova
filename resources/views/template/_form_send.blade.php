<!-- Name Form Input -->
<div class="form-group">
    {!! Form::label('name', 'Nome') !!}
    <br />
    {{ $template->name }}
</div>

<!-- Subject Form Input -->
<div class="form-group">
    {!! Form::label('subject', 'Assunto') !!}
    <br />
    {{ $template->subject }}
</div>

<!-- Body Form Input -->
<div class="form-group">
    {!! Form::label('body', 'Conteúdo') !!}
    <br />
    {!! $template->body !!}
</div>

<!-- email Form Input -->
<div class="form-group @if ($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Destinatário') !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
    {!! Form::hidden('template_id', $template->template_id) !!}
    @if ($errors->has('email')) <p class="form-text text-muted">{{ $errors->first('email') }}</p> @endif
</div>