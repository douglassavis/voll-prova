@extends('layouts.master')

@section('title', 'Cadastrar template')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3>Cadastrar template</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('templates.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Voltar</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => ['templates.store'] ]) !!}
                @include('template._form')
                <!-- Submit Form Button -->
                {!! Form::submit('Salvar', ['class' => 'btn btn-sm btn-voll']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection