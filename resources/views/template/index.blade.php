@extends('layouts.master')

@section('title', 'Templates')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} template(s)</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('add_templates')
                <a href="{{ route('templates.create') }}" class="btn btn-voll btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Novo</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Subject</th>
                <th>Created By</th>
                <th>Created At</th>
                @can('edit_templates', 'delete_templates')
                <th class="text-center">Actions</th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @foreach($result as $item)
                <tr>
                    <td>{{ $item->template_id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->subject }}</td>
                    <td>{{ $item->create_user->name }}</td>
                    <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>

                    @can('edit_templates')
                    <td class="text-center">
                        @include('shared._actions', [
                            'entity' => 'templates',
                            'id' => $item->template_id
                        ])
                    </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>

@endsection