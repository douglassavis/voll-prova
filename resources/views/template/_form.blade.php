<!-- Name Form Input -->
<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Nome') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
    @if ($errors->has('name')) <p class="form-text text-muted">{{ $errors->first('name') }}</p> @endif
</div>

<!-- Subject Form Input -->
<div class="form-group @if ($errors->has('subject')) has-error @endif">
    {!! Form::label('subject', 'Assunto') !!}
    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject']) !!}
    @if ($errors->has('subject')) <p class="form-text text-muted">{{ $errors->first('subject') }}</p> @endif
</div>

<!-- Body Form Input -->
<div class="form-group @if ($errors->has('body')) has-error @endif">
    {!! Form::label('body', 'Conteúdo') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control', 'placeholder' => 'Body']) !!}
    @if ($errors->has('body')) <p class="form-text text-muted">{{ $errors->first('body') }}</p> @endif
</div>