@extends('layouts.master')

@section('title', 'Enviar template - ' . $template->name)

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3>Enviar - {{ $template->name }}</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('templates.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Voltar</a>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::model($template, ['method' => 'POST', 'route' => 'templates.send_email.post' ]) !!}
                            @include('template._form_send')
                            <!-- Submit Form Button -->
                            {!! Form::submit('Enviar', ['class' => 'btn btn-sm btn-voll']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection