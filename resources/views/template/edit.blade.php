@extends('layouts.master')

@section('title', 'Editar template - ' . $template->name)

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3>Editar - {{ $template->name }}</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('templates.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Voltar</a>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::model($template, ['method' => 'PUT', 'route' => ['templates.update',  $template->template_id ] ]) !!}
                            @include('template._form')
                            <!-- Submit Form Button -->
                            {!! Form::submit('Salvar', ['class' => 'btn btn-sm btn-voll']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection