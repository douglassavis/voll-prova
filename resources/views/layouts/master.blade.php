<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-4.2.1/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-5.6.3/css/all.css') }}">

    <style>
        .result-set { margin-top: 1em }
    </style>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

    <!--<nav class="navbar navbar-expand-lg navbar-static-top navbar-dark background-02">-->
    <nav class="navbar navbar-expand-lg navbar-dark background-02">
        <div class="container">

            <a class="navbar-brand" href="/">
                <img src="{{ asset('img/logo.png') }}" width="30" height="30" class="d-inline-block align-top" alt="VOLL" /> Gestão de templates de e-mail
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-end" id="navbarToggler">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    @if (Auth::check())

                        @can('view_templates')
                            <li class="nav-item {{ Request::is('templates*') ? 'active' : '' }}">
                                <a href="{{ route('templates.index') }}" class="nav-link">
                                    <span class="text-success glyphicon glyphicon-text-background"></span> Templates
                                </a>
                            </li>
                        @endcan

                        @can('view_templates')
                            <li class="nav-item {{ Request::is('queue*') ? 'active' : '' }}">
                                <a href="{{ route('queue.index') }}" class="nav-link">
                                    <span class="text-success glyphicon glyphicon-text-background"></span> Queue
                                </a>
                            </li>
                        @endcan

                        {{--
                        @can('view_users')
                            <li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
                                <a href="{{ route('users.index') }}" class="nav-link">
                                    <span class="text-info glyphicon glyphicon-user"></span> Users
                                </a>
                            </li>
                        @endcan
                        --}}

                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li class="nav-item {{ Request::is('login') ? 'active' : '' }}">
                            <a href="{{ route('login') }}" class="nav-link">Login</a>
                        </li>
                        <li class="nav-item {{ Request::is('register') ? 'active' : '' }}">
                            <a href="{{ route('register') }}" class="nav-link">Registrar</a>
                        </li>
                    @else

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                                <span class="label label-success comment">({{ Auth::user()->roles->pluck('name')->first() }})</span>
                                <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="glyphicon glyphicon-log-out"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>

                        </li>
                    @endif
                </ul>
            </div>

        </div>
    </nav>

    <main role="main" class="container">
        <div id="flash-msg">
            @include('flash::message')
        </div>
        @yield('content')
    </main>

    <!-- Scripts -->
    <script src="{{ asset('vendor/jquery-3.3.1/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js') }}"></script>

    @stack('scripts')

    <script>
        $(function () {
            // flash auto hide
            $('#flash-msg .alert').not('.alert-danger, .alert-important').delay(6000).slideUp(500);

           @yield('javascript') 

        })
    </script>
</body>
</html>