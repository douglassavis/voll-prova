@extends('layouts.master')

@section('title', 'Queue')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} email(s)</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('send_email_users')
                <a class="btn btn-voll btn-sm" id="attempt-action" href="{{ route('queue.attempt') }}" 
onclick="event.preventDefault(); document.getElementById('attempt-full').submit();"
                >
                    <i class="glyphicon glyphicon-plus-sign"></i> Processar fila
                </a>
                <form id="attempt-full" action="{{ route('queue.attempt') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Subject</th>
                <th>Recipient</th>
                <th>Status</th>
                <th>Created By</th>
                <th>Created At</th>
                {{--
                @can('edit_templates', 'delete_templates')
                <th class="text-center">Actions</th>
                @endcan
                --}}
            </tr>
            </thead>
            <tbody>
            @foreach($result as $item)
                <tr>
                    <td>{{ $item->template_id }}</td>
                    <td>{{ $item->template->subject }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ ($item->status == 0) ? 'Não enviado' : 'Enviado' }}</td>
                    <td>{{ $item->create_user->name }}</td>
                    <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>

                    {{--
                    @can('edit_templates')
                    <td class="text-center">
                        @include('shared._actions', [
                            'entity' => 'queue',
                            'id' => $item->queue_id
                        ])
                    </td>
                    @endcan
                    --}}
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>

@endsection


@section('javascript')

    $('#attempt-action').on('click', function(e) {

        e.preventDefault();

        var token = $(this).find('input[name="_token"]').val();
        $('#attempt-action').attr('disabled', 'disabled');

        $.ajax({
            type: "POST",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: "/queue/attempt",
            dataType: "text",
            headers: {
                'X-CSRF-TOKEN': token
            }
        })
        .done( function( data ) {
            //location.reload;
        })
        .error( function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        });

    });

@endsection