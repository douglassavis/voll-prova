<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queues', function (Blueprint $table) {
            $table->increments('queue_id');

            $table->integer('template_id')->unsigned();
            $table->string('email', 100);
            $table->tinyInteger('status');
            $table->dateTime('last_attempt')->nullable();
            
            $table->integer('created_user_id')->unsigned();
            $table->integer('attempt_user_id')->unsigned()->nullable();
            $table->integer('deleted_user_id')->unsigned()->nullable();
            $table->dateTime('created_at');
            $table->softDeletes();

            $table->foreign('created_user_id')->references('user_id')->on('users');
            $table->foreign('attempt_user_id')->references('user_id')->on('users');
            $table->foreign('deleted_user_id')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queues');
    }
}
