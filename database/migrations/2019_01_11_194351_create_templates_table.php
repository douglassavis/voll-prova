<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->increments('template_id');

            $table->string('name', 50);
            $table->string('subject', 100);
            $table->text('body');

            $table->integer('created_user_id')->unsigned();
            $table->integer('updated_user_id')->unsigned()->nullable();
            $table->integer('deleted_user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_user_id')->references('user_id')->on('users');
            $table->foreign('updated_user_id')->references('user_id')->on('users');
            $table->foreign('deleted_user_id')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates');
    }
}
