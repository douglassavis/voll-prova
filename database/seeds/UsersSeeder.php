<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'name' => 'Douglas Sávis de Souza',
        	'email' => 'douglassavis@yahoo.com.br',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('super_admin');


        $user = User::create([
        	'name' => 'Yuri Lopes',
        	'email' => 'yuri.lopes@govoll.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('super_admin');


        $user = User::create([
        	'name' => 'Admin',
        	'email' => 'super_admin@douglassavis.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('super_admin');


        $user = User::create([
        	'name' => 'Assistente Templates',
        	'email' => 'templates_assistant@douglassavis.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('templates_assistant');


        $user = User::create([
        	'name' => 'Master Templates',
        	'email' => 'templates_master@douglassavis.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('templates_master');


        $user = User::create([
        	'name' => 'Assistente Usuários',
        	'email' => 'users_assistant@douglassavis.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('users_assistant');


        $user = User::create([
        	'name' => 'Master Usuários',
        	'email' => 'users_master@douglassavis.com',
            'password' => bcrypt('123456'),
        ]);
        $user->assignRole('users_master');
    }
}
