<?php

use Illuminate\Database\Seeder;
use App\Template;
use App\User;

class TemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $template = Template::create([
            'name' => 'Welcome',
            'subject' => 'Seja bem vindo',
            'body' => '<h1 style="font-size: 3rem;font-weight:bolder;color:#ff6302">Seja bem vindo a plataforma empresarial Voll</h1><p style="margin-top:30px;font-size:11px;color:gray">Para maiores informações acesse <a href="https://govoll.com/" target="_blank">https://govoll.com/</a></p>',
            'created_user_id' => 1,
        ]);
    }
}
