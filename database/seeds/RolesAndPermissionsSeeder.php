<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'add_templates']);
        Permission::create(['name' => 'view_templates']);
        Permission::create(['name' => 'edit_templates']);
        Permission::create(['name' => 'delete_templates']);
        Permission::create(['name' => 'preview_templates']);

        Permission::create(['name' => 'add_users']);
        Permission::create(['name' => 'view_users']);
        Permission::create(['name' => 'edit_users']);
        Permission::create(['name' => 'delete_users']);
        Permission::create(['name' => 'change_users_password']);

        Permission::create(['name' => 'send_email_users']);

        Permission::create(['name' => 'start_queues']);
        //Permission::create(['name' => 'view_queues']);
        Permission::create(['name' => 'delete_queues']);

        // create roles and assign created permissions

		// Templates Permissions
        $role = Role::create(['name' => 'templates_assistant']);
        // $role->givePermissionTo([
        // 	'add_templates',
        // 	'view_templates',
        // 	'edit_templates',
        // 	'preview_templates',
        // 	'send_email_users',
        // ]);

        $role = Role::create(['name' => 'templates_master']);
        // $role->givePermissionTo([
        // 	'add_templates',
        // 	'view_templates',
        // 	'edit_templates',
        // 	'preview_templates',
        // 	'delete_templates',
        // 	'send_email_users',
        // 	'start_queues',
        // 	'view_queues',
        // 	'delete_queues',
        // ]);

        // Users Permissions
        $role = Role::create(['name' => 'users_assistant']);
        // $role->givePermissionTo([
        // 	'view_users',
        // 	'edit_users',
        // 	'change_users_password',
        // ]);

        $role = Role::create(['name' => 'users_master']);
        // $role->givePermissionTo([
        // 	'add_users',
        // 	'view_users',
        // 	'edit_users',
        // 	'delete_users',
        // 	'change_users_password',
        // ]);

        // All Permissions
        $role = Role::create(['name' => 'super_admin']);
        //$role->givePermissionTo(Permission::all());
    }
}
